<?php 
    require_once 'fight.php';

    class Hewan {
        public $nama;
        public $darah;
        public $jumlahKaki;
        public $keahlian;

        public function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackPower, $defencePower)
        {
            $this->nama = $nama;
            $this->darah = $darah;
            $this->jumlahKaki = $jumlahKaki;
            $this->keahlian = $keahlian;
            $this->attackPower = $attackPower;
            $this->defencePower = $defencePower;
        }

        public function atraksi()
        {
            return $this->nama . ' sedang ' . $this->keahlian;
        }
    }

    class Elang extends Hewan {
        public function getInfoHewan()
        {
            echo 
            'Jenis Hewan = '. $this->nama 
            .'<br>'. 
            'Darah = '. $this->darah 
            .'<br>'.  
            'Jumlah Kaki = '. $this->jumlahKaki 
            .'<br>'. 
            'Keahlian = '. $this->keahlian
            .'<br>'.
            'Attack Power = '. $this->attackPower
            .'<br>'.
            'Defence Power = '. $this->defencePower
            .'<br>';
        }
    }
    
    class Harimau extends Hewan {
        public function getInfoHewan()
        {
            echo 
            'Jenis Hewan = '. $this->nama 
            .'<br>'. 
            'Darah = '. $this->darah 
            .'<br>'.  
            'Jumlah Kaki = '. $this->jumlahKaki 
            .'<br>'. 
            'Keahlian = '. $this->keahlian
            .'<br>'.
            'Attack Power = '. $this->attackPower
            .'<br>'.
            'Defence Power = '. $this->defencePower
            .'<br>';
        }
    }
?>