<?php 
    require_once 'hewan.php';
    require_once 'fight.php';
    
    $elang = new Elang('elang_1', 50, 2, "terbang tinggi", 10, 5);
    $harimau = new Harimau('harimau_2', 50, 4, "lari cepat", 7, 8);
    $fight = new Fight();

    echo '<h1>Informasi Hewan</h1>';
    $elang->getInfoHewan();
    echo '<br>';
    $harimau->getInfoHewan();
    echo '<br>';
    
    echo '<h1>Atraksi Hewan</h1>';
    echo $elang->atraksi();
    echo '<br>';
    echo $harimau->atraksi();
    echo '<br>';

    echo $fight->serang($elang->nama, $harimau->nama);
    echo '<br>';
    echo $fight->diserang($harimau->nama, $harimau->darah, $elang->attackPower, $harimau->defencePower);
?>